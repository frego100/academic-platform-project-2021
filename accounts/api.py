from .serializers import UserSerializer, CreateUserSerializer, CreateStudentUserSerializer, LoginUserSerializer
from rest_framework import generics, permissions
from rest_framework.authtoken.serializers import AuthTokenSerializer
from django.contrib.auth import login
from knox.views import LoginView as KnoxLoginView
from knox.models import AuthToken
from rest_framework.response import Response

##############################################
# CREATED BY: AUT-OO05
# CREATED DATE: 12-07-2021
# MODIFIED BY: --
# MODIFIED DATE: --
# OBS: --
##############################################

# Generic Register API


class RegisterAPI(generics.GenericAPIView):
    serializer_class = CreateUserSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        email = serializer.save()
        return Response({
            "email": UserSerializer(email, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(email)[1]
        })


# Student Register API

class StudentRegisterAPI(generics.GenericAPIView):
    serializer_class = CreateStudentUserSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        email = serializer.save()
        return Response({
            "email": UserSerializer(email, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(email)[1]
        })


# Login API

class LoginAPI(KnoxLoginView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        serializer = LoginUserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        if user.last_login is None:
            user.first_login = True
            user.save()

        elif user.first_login:
            user.first_login = False
            user.save()

        login(request, user)
        return super().post(request, format=None)
