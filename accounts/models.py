from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.core.validators import RegexValidator
from django.db.models import Q
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.dispatch.dispatcher import NO_RECEIVERS
from rest_framework.authtoken.models import Token
from django.db.models.signals import post_save

import random
import os

##############################################
# CREATED BY: AUT-OO05
# CREATED DATE: 12-07-2021
# MODIFIED BY: --
# MODIFIED DATE: --
# OBS: --
##############################################


class UserManager(BaseUserManager):
    def create_user(self, email, password=None, is_staff=False, is_active=True, is_admin=False,
                    name=None, first_last_name=None, second_last_name=None, desired_university=None, education_profile=None):
        if not email:
            raise ValueError('users must have a email')
        if not password:
            raise ValueError('user must have a password')

        user_obj = self.model(
            email=email
        )
        user_obj.set_password(password)
        user_obj.staff = is_staff
        user_obj.admin = is_admin
        user_obj.active = is_active
        # setting custom fieds
        user_obj.name = name
        user_obj.first_last_name = first_last_name
        user_obj.second_last_name = second_last_name
        user_obj.desired_university = desired_university
        user_obj.education_profile = education_profile
        user_obj.save(using=self._db)
        return user_obj

    def create_student_user(self, phone, email, password, name, first_last_name,
                            second_last_name, desired_university, education_profile):
        user = self.create_user(
            email,
            password=password,
            is_staff=False,
        )

        return user

    def create_staffuser(self, email, password=None):
        user = self.create_user(
            email,
            password=password,
            is_staff=True,


        )
        return user

    def create_superuser(self, email, password=None):
        user = self.create_user(
            email,
            password=password,
            is_staff=True,
            is_admin=True,


        )
        return user


class User(AbstractBaseUser):
    phone_regex = RegexValidator(  # regex=r'^(\+\d{1,3})?-?\s?\d{9,14}'
        regex=r'^(\+\d{1,3})?-?\s?\d{9,14}', message="Phone number must be entered in the format: '+999999999'. Up to 14 digits allowed.")
    phone = models.CharField(
        validators=[phone_regex], max_length=17, blank=True, null=True)
    email = models.EmailField(
        verbose_name='email address', max_length=255, unique=True)
    name = models.CharField(max_length=30, blank=True, null=True)
    first_last_name = models.CharField(max_length=30, blank=True, null=True)
    second_last_name = models.CharField(max_length=30, blank=True, null=True)
    education_profile = models.CharField(max_length=40, blank=True, null=True)
    desired_university = models.CharField(
        max_length=120, blank=True, null=True)
    #standard = models.CharField(max_length=3, blank=True, null=True)
    #score = models.IntegerField(default=16)
    first_login = models.BooleanField(default=False)
    active = models.BooleanField(default=True)
    staff = models.BooleanField(default=False)
    admin = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self):
        return self.email

    def get_full_name(self):
        return self.phone

    def get_short_name(self):
        return self.phone

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):

        return True

    @property
    def is_staff(self):
        return self.staff

    @property
    def is_admin(self):
        return self.admin

    @property
    def is_active(self):
        return self.active
