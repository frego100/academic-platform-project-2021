from .api import RegisterAPI
from django.urls import path
from knox.views import LogoutView, LogoutAllView
from .api import LoginAPI
from .api import StudentRegisterAPI
from django.urls import path

# register endpoint for any user by the moment
urlpatterns = [
    #path('usr/register/', RegisterAPI.as_view(), name='user register'),
]

urlpatterns += [
    path('auth/login/', LoginAPI.as_view(), name='login'),
    path('auth/logout/', LogoutView.as_view(),
         name='logout the current session'),
    path('auth/logoutall/', LogoutAllView.as_view(),
         name='logout all sessions for the current user'),
]

# student register endpoint

urlpatterns += [
    path('usr/student/register/',
         StudentRegisterAPI.as_view(), name='student register')
]
